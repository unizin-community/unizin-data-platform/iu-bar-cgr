SELECT NOW();

DROP TABLE IF EXISTS cgr.learner_activity;

create table cgr.learner_activity (
  key text -- needed to parse course data
  , learner_activity_id text -- Needed to join learner activity result data.
  , points_possible numeric
  , due_date timestamp
);

insert into cgr.learner_activity (
  key
  , learner_activity_id
  , points_possible
  , due_date
)

select
  cd.sis_source_id AS key
  , la.lms_int_id AS learner_activity_id
  , la.points_possible AS points_possible
  , la.due_date AS due_date

FROM
  lms.learner_activity AS la

INNER JOIN
  cd.course_dim AS cd ON la.course_offering_id = cd.id::text

WHERE
  la.status = 'published' -- Only published
  AND la.grade_type = 'points'
;

CREATE INDEX ON cgr.learner_activity(learner_activity_id);

SELECT COUNT(1) from lms.learner_activity;
SELECT COUNT(1) from cgr.learner_activity;

SELECT NOW();
