/*
  Person
*/

DROP TABLE IF EXISTS cgr.person;

create table cgr.person (
  person_id TEXT
  , sex TEXT
  , gender TEXT
  , is_black_african_american BOOLEAN
  , is_american_indian_alaska_native BOOLEAN
  , is_asian BOOLEAN
  , is_hawaiian_pacific_islander BOOLEAN
  , is_hispanic_latino BOOLEAN
  , is_white BOOLEAN
  , is_other_american BOOLEAN
  , is_no_race_indicated BOOLEAN
  , is_first_generation_hed_student BOOLEAN
  , is_english_first_language BOOLEAN
  , was_veteran_at_admissions BOOLEAN
  , zip_code_first_us_permanent_residence TEXT
  , zip_code_at_time_of_application TEXT
  , neighborhood_cb_cluster_code_at_time_of_application TEXT
  , estimated_gross_family_income TEXT
  , hs_cb_cluster_code TEXT
  , hs_ceeb_code TEXT
  , hs_zip_code TEXT
  , hs_state_code TEXT
  , hs_city_name TEXT
  , hs_gpa NUMERIC
  , hs_percentile_rank INTEGER
);

INSERT INTO cgr.person (
  person_id
  , sex
  , gender
  , is_black_african_american
  , is_american_indian_alaska_native
  , is_asian
  , is_hawaiian_pacific_islander
  , is_hispanic_latino
  , is_white
  , is_other_american
  , is_no_race_indicated
  , is_first_generation_hed_student
  , is_english_first_language
  , was_veteran_at_admissions
  , zip_code_first_us_permanent_residence
  , zip_code_at_time_of_application
  , neighborhood_cb_cluster_code_at_time_of_application
  , estimated_gross_family_income
  , hs_cb_cluster_code
  , hs_ceeb_code
  , hs_zip_code
  , hs_state_code
  , hs_city_name
  , hs_gpa
  , hs_percentile_rank
)

SELECT
  p.sis_int_id person_id
  , p.sex
  , p.gender
  , p.is_black_african_american
  , p.is_american_indian_alaska_native
  , p.is_asian
  , p.is_hawaiian_pacific_islander
  , p.is_hispanic_latino
  , p.is_white
  , p.is_other_american
  , p.is_no_race_indicated
  , p.is_first_generation_hed_student
  , p.is_english_first_language
  , p.was_veteran_at_admissions
  , p.zip_code_first_us_permanent_residence
  , p.zip_code_at_time_of_application
  , p.neighborhood_cb_cluster_code_at_time_of_application
  , p.estimated_gross_family_income
  , p.hs_cb_cluster_code
  , p.hs_ceeb_code
  , p.hs_zip_code
  , p.hs_state_code
  , p.hs_city_name
  , p.hs_gpa
  , p.hs_percentile_rank
FROM
  sis.person AS p
;

CREATE INDEX ON cgr.person(person_id);
