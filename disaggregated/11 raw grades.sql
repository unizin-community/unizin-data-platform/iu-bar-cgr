/*
  This is only required for IU, since
  their course grade data is not yet
  making it through the batch-ingest process.
*/

DROP TABLE IF EXISTS cgr.raw_course_grade;

CREATE TABLE cgr.raw_course_grade (
  sis_int_id TEXT,
  sis_ext_id TEXT,
  person_id TEXT NOT NULL,
  course_section_id TEXT NOT NULL,
  grade_inputted_by_instructor TEXT,
  grade_on_official_transcript TEXT,
  grade_points_per_credit NUMERIC,
  grade_points NUMERIC,
  is_course_grade_in_gpa BOOLEAN,
  gpa_cumulative_excluding_course_grade NUMERIC,
  gpa_current_excluding_course_grade NUMERIC,
  grading_basis TEXT
);

CREATE INDEX ON cgr.raw_course_grade(course_section_id);

\copy cgr.raw_course_grade FROM '/path/to/raw/grades' DELIMITER ',' CSV HEADER;
