/*
  Used to map the SIS and LMS person identifiers,
  since the CSE data comes from SIS and the LMS
  data comes from the LMS!
*/

DROP TABLE IF EXISTS cgr.person_map;

create table cgr.person_map (
  lms_int_id text
  , sis_ext_id text
);

INSERT INTO cgr.person_map (
  lms_int_id
  , sis_ext_id
)

SELECT
  lms_int_id
  , sis_ext_id
FROM
  lms.person
WHERE
  sis_ext_id IS NOT NULL
;

CREATE INDEX ON cgr.person_map(lms_int_id);
CREATE INDEX ON cgr.person_map(sis_ext_id);
