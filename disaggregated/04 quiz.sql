SELECT NOW();

DROP TABLE IF EXISTS cgr.quiz;

create table cgr.quiz (
  key TEXT -- Needed to parse course data
  , quiz_id TEXT -- Needed to join quiz result data
  , points_possible NUMERIC
  , due_date TIMESTAMP
);

insert into cgr.quiz (
  key
  , quiz_id
  , points_possible
  , due_date
)

select
  cd.sis_source_id AS key -- This will be null if the LMS course was created natively.
  , q.lms_int_id AS quiz_id
  , q.points_possible AS points_possible
  , q.due_date AS quiz_due_date

FROM
  lms.quiz q

INNER JOIN
  cd.course_dim AS cd ON q.course_offering_id = cd.id::text

WHERE
  q.status = 'published' -- Only published quizzes
  AND (
    q.type = 'assignment'    OR
    q.type = 'graded_survey' OR
    q.type = 'survey'
  ) -- Exclude practice quizzes and surveys.
;

CREATE INDEX ON cgr.quiz(quiz_id);

SELECT COUNT(1) from lms.quiz;
SELECT COUNT(1) from cgr.quiz;

SELECT NOW();
