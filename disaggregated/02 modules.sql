/*
  Module items in course
*/

DROP TABLE IF EXISTS cgr.modules;

create table cgr.modules (
  key TEXT
  , total_module_items NUMERIC
  , total_modules NUMERIC
  , avg_module_items_per_module NUMERIC
);

INSERT INTO cgr.modules (
  key
  , total_module_items
  , total_modules
  , avg_module_items_per_module
)

SELECT
  cd.sis_source_id AS key
  , SUM(mis.total_module_items) AS total_module_items
  , COUNT(1) AS total_modules
  , SUM(mis.total_module_items)::numeric /  COUNT(1) AS avg_module_items_per_module

FROM
  (
    SELECT
      m.course_offering_id
      , m.lms_int_id module_id
      , COUNT(1) AS total_module_items

    FROM
      lms.module_item mi

    INNER JOIN
      lms.module m ON mi.module_id=m.lms_int_id

    WHERE
      m.status = 'active' -- Only active modules
      AND mi.status = 'active' -- Only active module items

    GROUP BY
      m.lms_int_id
      , m.course_offering_id
  ) mis

INNER JOIN
  cd.course_dim AS cd ON mis.course_offering_id = cd.id::text

GROUP BY
  cd.sis_source_id
;

CREATE INDEX ON cgr.modules(key);
