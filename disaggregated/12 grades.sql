DROP TABLE IF EXISTS cgr.course_grades;

CREATE table cgr.course_grades (
  person_id TEXT
  , key TEXT
  , grade_inputted_by_instructor TEXT
  , grade_on_official_transcript TEXT
  , grade_points_per_credit NUMERIC
  , grade_points NUMERIC
  , is_course_grade_in_gpa BOOLEAN
  , gpa_cumulative_excluding_course_grade NUMERIC
  , gpa_current_excluding_course_grade NUMERIC
  , grading_basis TEXT
);

INSERT INTO cgr.course_grades (
  person_id
  , key
  , grade_inputted_by_instructor
  , grade_on_official_transcript
  , grade_points_per_credit
  , grade_points
  , is_course_grade_in_gpa
  , gpa_cumulative_excluding_course_grade
  , gpa_current_excluding_course_grade
  , grading_basis
)

SELECT
  rcg.person_id AS person_id
  , cs.sis_ext_id AS key
  , rcg.grade_inputted_by_instructor AS grade_inputted_by_instructor
  , rcg.grade_on_official_transcript AS grade_on_official_transcript
  , rcg.grade_points_per_credit AS grade_points_per_credit
  , rcg.grade_points AS grade_points
  , rcg.is_course_grade_in_gpa AS is_course_grade_in_gpa
  , rcg.gpa_cumulative_excluding_course_grade AS gpa_cumulative_excluding_course_grade
  , rcg.gpa_current_excluding_course_grade AS gpa_current_excluding_course_grade
  , rcg.grading_basis AS grading_basis

FROM
  cgr.raw_course_grade rcg

INNER JOIN
  sis.course_section cs ON rcg.course_section_id = cs.sis_int_id

GROUP BY
  rcg.person_id
  , cs.sis_ext_id
  , rcg.course_section_id
  , rcg.grade_inputted_by_instructor
  , rcg.grade_on_official_transcript
  , rcg.grade_points_per_credit
  , rcg.grade_points
  , rcg.is_course_grade_in_gpa
  , rcg.gpa_cumulative_excluding_course_grade
  , rcg.gpa_current_excluding_course_grade
  , rcg.grading_basis
;

CREATE INDEX ON cgr.course_grades(person_id);
CREATE INDEX ON cgr.course_grades(key);
