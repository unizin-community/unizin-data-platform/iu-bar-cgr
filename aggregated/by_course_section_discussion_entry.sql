WITH course_section_enrollments AS (
  SELECT
    cse.key
    , COUNT(1) num_student_enrollments
  FROM
    cgr_disaggregated.course_section_enrollment cse
  WHERE
    role = 'Student'
  GROUP BY
    key
),

discussion_entries_with_date_in_term AS (
  SELECT
    de.person_id
      , de.key
      , de.discussion_id
      , cs.instruction_begin_date
      , de.created_date
      , CASE WHEN de.created_date IS NULL THEN TRUE ELSE FALSE END AS null_created_date
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 7 DAY) THEN TRUE ELSE FALSE END as by_week_1
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 14 DAY) THEN TRUE ELSE FALSE END as by_week_2
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 21 DAY) THEN TRUE ELSE FALSE END as by_week_3
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 28 DAY) THEN TRUE ELSE FALSE END as by_week_4
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 35 DAY) THEN TRUE ELSE FALSE END as by_week_5
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 42 DAY) THEN TRUE ELSE FALSE END as by_week_6
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 49 DAY) THEN TRUE ELSE FALSE END as by_week_7
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 56 DAY) THEN TRUE ELSE FALSE END as by_week_8
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 63 DAY) THEN TRUE ELSE FALSE END as by_week_9
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 70 DAY) THEN TRUE ELSE FALSE END as by_week_10
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 77 DAY) THEN TRUE ELSE FALSE END as by_week_11
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 84 DAY) THEN TRUE ELSE FALSE END as by_week_12
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 91 DAY) THEN TRUE ELSE FALSE END as by_week_13
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 98 DAY) THEN TRUE ELSE FALSE END as by_week_14
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 105 DAY) THEN TRUE ELSE FALSE END as by_week_15
      , CASE WHEN de.created_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 112 DAY) THEN TRUE ELSE FALSE END as by_week_16
      , CASE WHEN de.created_date >  DATETIME_ADD(cs.instruction_begin_date, INTERVAL 112 DAY) THEN TRUE ELSE FALSE END as beyond_week_16

  FROM
    cgr_disaggregated.discussion_entry de

  INNER JOIN
    cgr_disaggregated.course_section AS cs ON de.key = cs.key
)

SELECT
  de.key AS key

  /* Total discussion entry features */
  , SUM(CASE WHEN de.by_week_1 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_1
  , SUM(CASE WHEN de.by_week_2 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_2
  , SUM(CASE WHEN de.by_week_3 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_3
  , SUM(CASE WHEN de.by_week_4 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_4
  , SUM(CASE WHEN de.by_week_5 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_5
  , SUM(CASE WHEN de.by_week_6 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_6
  , SUM(CASE WHEN de.by_week_7 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_7
  , SUM(CASE WHEN de.by_week_8 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_8
  , SUM(CASE WHEN de.by_week_9 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_9
  , SUM(CASE WHEN de.by_week_10 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_10
  , SUM(CASE WHEN de.by_week_11 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_11
  , SUM(CASE WHEN de.by_week_12 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_12
  , SUM(CASE WHEN de.by_week_13 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_13
  , SUM(CASE WHEN de.by_week_14 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_14
  , SUM(CASE WHEN de.by_week_15 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_15
  , SUM(CASE WHEN de.by_week_16 IS TRUE THEN 1 ELSE 0 END) AS total_entries_by_week_16
  , SUM(CASE WHEN de.beyond_week_16 IS TRUE THEN 1 ELSE 0 END) AS total_entries_beyond_week_16
  , COUNT(1) AS total_entries

  /* Average discussion entry features */
  , SUM(CASE WHEN de.by_week_1 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_1
  , SUM(CASE WHEN de.by_week_2 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_2
  , SUM(CASE WHEN de.by_week_3 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_3
  , SUM(CASE WHEN de.by_week_4 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_4
  , SUM(CASE WHEN de.by_week_5 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_5
  , SUM(CASE WHEN de.by_week_6 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_6
  , SUM(CASE WHEN de.by_week_7 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_7
  , SUM(CASE WHEN de.by_week_8 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_8
  , SUM(CASE WHEN de.by_week_9 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_9
  , SUM(CASE WHEN de.by_week_10 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_10
  , SUM(CASE WHEN de.by_week_11 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_11
  , SUM(CASE WHEN de.by_week_12 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_12
  , SUM(CASE WHEN de.by_week_13 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_13
  , SUM(CASE WHEN de.by_week_14 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_14
  , SUM(CASE WHEN de.by_week_15 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_15
  , SUM(CASE WHEN de.by_week_16 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_by_week_16
  , SUM(CASE WHEN de.beyond_week_16 IS TRUE THEN 1 ELSE 0 END) / cse.num_student_enrollments AS avg_entries_per_student_beyond_week_16
  , COUNT(1) / cse.num_student_enrollments AS avg_entries_per_student

FROM
  discussion_entries_with_date_in_term AS de

INNER JOIN course_section_enrollments AS cse ON de.key = cse.key

GROUP BY
  de.key
  , cse.num_student_enrollments
;
