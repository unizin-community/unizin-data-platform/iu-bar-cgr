/*
  Computes the % of rows that have the
  referenced fields from the dataset.
*/

SELECT
  SUM(CASE WHEN grade_on_official_transcript IS NOT NULL THEN 1 ELSE 0 END) / COUNT(1) perc_has_grade
  , SUM(CASE WHEN hs_gpa IS NOT NULL THEN 1 ELSE 0 END) / COUNT(1) perc_has_hs_gpa
  , SUM(CASE WHEN hs_zip_code IS NOT NULL THEN 1 ELSE 0 END) / COUNT(1) perc_has_hs_zip_code
  , SUM(CASE WHEN hs_percentile_rank IS NOT NULL THEN 1 ELSE 0 END) / COUNT(1) perc_has_hs_percentile_rank
  , SUM(CASE WHEN role IS NOT NULL THEN 1 ELSE 0 END) / COUNT(1) perc_has_role
  , SUM(CASE WHEN avg_module_items_per_module IS NOT NULL THEN 1 ELSE 0 END) / COUNT(1) perc_has_avg_module_items_per_module
  , SUM(CASE WHEN avg_course_learner_activity_score_by_week_3 IS NOT NULL THEN 1 ELSE 0 END) / COUNT(1) perc_has_avg_course_learner_activity_score_by_week_3
  , SUM(CASE WHEN avg_course_quiz_score_by_week_3 IS NOT NULL THEN 1 ELSE 0 END) / COUNT(1) perc_has_avg_course_quiz_score_by_week_3
  , SUM(CASE WHEN avg_entries_per_student_by_week_3 IS NOT NULL THEN 1 ELSE 0 END) / COUNT(1) perc_has_avg_entries_per_student_by_week_3
  , SUM(CASE WHEN avg_student_learner_activity_score_by_week_3 IS NOT NULL THEN 1 ELSE 0 END) / COUNT(1) perc_has_avg_student_learner_activity_score_by_week_3
  , SUM(CASE WHEN avg_student_quiz_score_by_week_3 IS NOT NULL THEN 1 ELSE 0 END) / COUNT(1) perc_has_avg_student_quiz_score_by_week_3
  , SUM(CASE WHEN total_student_entries_by_week_3 IS NOT NULL THEN 1 ELSE 0 END) / COUNT(1) perc_has_total_student_entries_by_week_3
FROM
  cgr_consolidated.final
;
