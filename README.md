# Course grade recommender

This repository contains SQL files that are used to generate a dataset for IU BAR's course grade model prediction project.

The directories in this project correspond to thematically related tables of data. Generally speaking, you'll want to look through them in this order:

1. **Disaggregated**. These queries are meant to be run in a UDP context store instance. Their purpose is to query the relevant data from the UDP context store's `entity` schema and then persist them in a `cgr` schema. From there, the Context store tables are expected to be brought into BigQuery, wherefrom the SQL in the remainder of this project's directories are run.

2. **Aggregated**. These queries roll up learner activity, quiz, and discussion metrics by course section and, also, by person in a course section. Their purpose is to generate metrics that are included in the final dataset.

3. **Consolidated**. These queries consolidate data across the disaggregated data and aggregated data (metrics) to generate a complete dataset. From the consolidated, indiviudal training sets can be generated.

4. **Training sets**. These queries generate subsets of the consolidated data to be used as training sets for a classifier.
