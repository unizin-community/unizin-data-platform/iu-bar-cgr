WITH quizzes_with_due_date_in_term AS (
  SELECT
    q.key
    , q.quiz_id
    , q.points_possible
    , cs.instruction_begin_date
    , q.due_date
    , CASE WHEN q.due_date IS NULL THEN TRUE ELSE FALSE END as null_due_date
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 7 DAY) THEN TRUE ELSE FALSE END as by_week_1
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 14 DAY) THEN TRUE ELSE FALSE END as by_week_2
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 21 DAY) THEN TRUE ELSE FALSE END as by_week_3
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 28 DAY) THEN TRUE ELSE FALSE END as by_week_4
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 35 DAY) THEN TRUE ELSE FALSE END as by_week_5
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 42 DAY) THEN TRUE ELSE FALSE END as by_week_6
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 49 DAY) THEN TRUE ELSE FALSE END as by_week_7
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 56 DAY) THEN TRUE ELSE FALSE END as by_week_8
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 63 DAY) THEN TRUE ELSE FALSE END as by_week_9
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 70 DAY) THEN TRUE ELSE FALSE END as by_week_10
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 77 DAY) THEN TRUE ELSE FALSE END as by_week_11
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 84 DAY) THEN TRUE ELSE FALSE END as by_week_12
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 91 DAY) THEN TRUE ELSE FALSE END as by_week_13
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 98 DAY) THEN TRUE ELSE FALSE END as by_week_14
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 105 DAY) THEN TRUE ELSE FALSE END as by_week_15
    , CASE WHEN q.due_date <= DATETIME_ADD(cs.instruction_begin_date, INTERVAL 112 DAY) THEN TRUE ELSE FALSE END as by_week_16
    , CASE WHEN q.due_date >  DATETIME_ADD(cs.instruction_begin_date, INTERVAL 112 DAY) THEN TRUE ELSE FALSE END as beyond_week_16

  FROM
    cgr_disaggregated.quiz q

  LEFT JOIN
    cgr_disaggregated.course_section AS cs ON q.key = cs.key
)

SELECT
  qr.person_id AS person_id
  , qr.key AS key
  , qr.quiz_id AS quiz_id
  , qr.started_date -- Not really valuable, but hey, you never know
  , qr.finished_date -- This is the one to use
  , q.due_date
  , q.instruction_begin_date

FROM
  cgr_disaggregated.quiz_result AS qr

INNER JOIN
  quizzes_with_due_date_in_term AS q ON qr.quiz_id = q.quiz_id
